import React from 'react';

import Navbar from 'react-bootstrap/Navbar';

import 'bootstrap/dist/css/bootstrap.min.css';
import './styles.css';
import logoItau from '../../assets/img/logo-itau.png';

const NavBar = () => {
    return (
        <Navbar variant="dark" className="cor-itau">
            <Navbar.Brand href="#home">
                <img
                    alt=""
                    src={logoItau}
                    width="30"
                    height="30"
                    className="d-inline-block align-top"
                />{' '}
            Itaú Gastos do Cartão
            </Navbar.Brand>
        </Navbar>
    )
}

export default NavBar;