import React, { useState, useEffect } from 'react';

import api from '../../services/api';

import Container from 'react-bootstrap/Container';
import Table from 'react-bootstrap/Table';
import Tab from 'react-bootstrap/Tab';
import Tabs from 'react-bootstrap/Tabs';

import 'bootstrap/dist/css/bootstrap.min.css';
import './styles.css';

const calendario = [
    "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho",
    "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"
]

const TabelaGeral = () => {

    const [lancamento, setLancamento] = useState([]);
    const [categoria, setCategoria] = useState([]);
    const totalMes = [];

    useEffect(() => {
        async function loadLancamento() {
            const resLancamento = await api.get('/lancamentos');
            setLancamento(resLancamento.data);
        }
        loadLancamento();

        async function loadCategoria() {
            const resCategoria = await api.get('/categorias');
            setCategoria(resCategoria.data);
        }
        loadCategoria();
    }, [])

    //TOTAL DOS MESES 
    for (let i = 0; i <= 12; i++) {
        totalMes.push(lancamento.filter(e => e.mes_lancamento === i)
            .reduce((soma, atual) => soma + atual.valor, 0))
    }

    //COLOCANDO NOME NAS CATEGORIAS
    categoria.map(eCat =>
        lancamento.map(e => (eCat.id === e.categoria) ? e.categoria = eCat.nome : "")
    );

    //ORDENANDO 
    lancamento.sort((a, b) => a.mes_lancamento - b.mes_lancamento);

    return (
        <Container>
            <Tabs
                defaultActiveKey="total"
                id="uncontrolled-tab-example"
                className="justify-content-center mt-2 mb-2">

                <Tab eventKey="total" title="Gastos por Mês">
                    <Table striped bordered hover responsive>
                        <thead>
                            <tr>
                                <th>Mês</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            {totalMes.map((e, i) => e ? (
                                <tr>
                                    <td>{calendario[i - 1]}</td>
                                    <td>{e.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })}</td>
                                </tr>
                            ) : "")}
                        </tbody>
                    </Table>
                </Tab>

                <Tab eventKey="geral" title="Gastos Detalhados">
                    <Table striped bordered hover responsive="sm">
                        <thead className="text-center">
                            <tr>
                                <th>Origem</th>
                                <th>Catergoria</th>
                                <th>Valor</th>
                                <th>Mês</th>
                            </tr>
                        </thead>
                        <tbody className="text-center">
                            {lancamento.map((e, i) => (
                                <tr>
                                    <td className="text-left">{e.origem}</td>
                                    <td>{e.categoria}</td>
                                    <td>{e.valor.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })}</td>
                                    <td>{calendario[e.mes_lancamento - 1]}</td>
                                </tr>
                            ))}
                        </tbody>
                    </Table>
                </Tab>
            </Tabs>
        </Container>
    )
}

export default TabelaGeral;





