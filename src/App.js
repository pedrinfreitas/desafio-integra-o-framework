import React from 'react';

import NavBar from './components/NavBar';
import TabelaGeral from './components/TabelaGeral';

import 'bootstrap/dist/css/bootstrap.min.css';
import './global-styles.css';

const App = () => ( 
    <div>
      <NavBar />
      <TabelaGeral /> 
    </div>
  );

export default App;
